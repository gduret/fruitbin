import cv2
import numpy as np


for im in range(4995):
    id_obj = 0.0
    res_all = []
    for obj in ['banana1_mask', 'orange2_mask', 'pear2_mask']:
        res = [id_obj]
        image = cv2.imread(f"{obj}/{im}.png", 0)
        image = image/255.0
        _, contours, _ = cv2.findContours(image.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        print(len(contours[0]))
        for i in range(len(contours[0])):
            x = contours[0][i][0][0]/640.0
            res.append(x)
            y = contours[0][i][0][1]/480.0
            res.append(y)
        id_obj += 1.0
        res_all.append(res)
    # res_ln = len(res_all)
    # matrix = np.array([res_all[0], res_all[1], res_all[2]]).reshape(3, 1)
    # np.savetxt(f'labels_all/{im}.txt', np.array(res_all).reshape((1,3)))

    a_file = open(f'labels_all/{im}.txt', "w")
    for row in res_all:
        np.savetxt(a_file, np.array(row).reshape(1, len(row)))

    a_file.close()
