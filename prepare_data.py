import shutil
import os

def reform_data(src, data_name, data_option, Nb_camera, World_begin, Nb_world):
    for i in range(World_begin, World_begin + Nb_world): # worlds
        src_meta = f"{src}/{i}/meta.json"
        dst_meta = f"{data_name}/Meta/{i}.json"
        shutil.copy(src_meta, dst_meta)
        for j in range(1, Nb_camera+1): # cameras
            count = ((i-1)*Nb_camera) + j
            print(count)
            if data_option == "ground_truth_rgb":
                files_img = os.listdir(f"{src}/{i}/grabber_{j}/color/image/")
                src_img = f"{src}/{i}/grabber_{j}/color/image/{files_img[0]}"
                dst_img = f"{data_name}/RGB/{count}.png"
            else:
                files_img = os.listdir(f"{src}/{i}/grabber_{j}/depth/image/")
                src_img = f"{src}/{i}/grabber_{j}/depth/image/{files_img[0]}"
                dst_img = f"{data_name}/RGB/{count}.png"
            shutil.copy(src_img, dst_img)

            files_id = os.listdir(f"{src}/{i}/grabber_{j}/{data_option}/id_map/")
            src_id = f"{src}/{i}/grabber_{j}/{data_option}/id_map/{files_id[0]}"
            dst_id = f"{data_name}/Instance_Segmentation/{count}.png"
            shutil.copy(src_id, dst_id)

            files_semantic = os.listdir(f"{src}/{i}/grabber_{j}/{data_option}/semantic_map/")
            src_semantic = f"{src}/{i}/grabber_{j}/{data_option}/semantic_map/{files_semantic[0]}"
            dst_semantic = f"{data_name}/Semantic_Segmentation/{count}.png"
            shutil.copy(src_semantic, dst_semantic)

            files_pose = os.listdir(f"{src}/{i}/grabber_{j}/{data_option}/3d_pose/")
            src_path_pose = f"{src}/{i}/grabber_{j}/{data_option}/3d_pose/{files_pose[0]}"
            dst_path_pose = f"{data_name}/Pose/{count}.json"
            shutil.copy(src_path_pose, dst_path_pose)

            files_occlusion = os.listdir(f"{src}/{i}/grabber_{j}/{data_option}/occlusion/")
            src_path_occlusion = f"{src}/{i}/grabber_{j}/{data_option}/occlusion/{files_occlusion[0]}"
            dst_path_occlusion = f"{data_name}/Occlusion/{count}.json"
            shutil.copy(src_path_occlusion, dst_path_occlusion)

            files_depth = os.listdir(f"{src}/{i}/grabber_{j}/depth/depth_map/")
            src_depth = f"{src}/{i}/grabber_{j}/depth/depth_map/{files_depth[0]}"
            dst_depth = f"{data_name}/Depth/{count}.tiff"
            shutil.copy(src_depth, dst_depth)

            files_bbox = os.listdir(f"{src}/{i}/grabber_{j}/{data_option}/2d_detection//")
            src_bbox = f"{src}/{i}/grabber_{j}/{data_option}/2d_detection/{files_bbox[0]}"
            dst_bbox = f"{data_name}/Bbox_2d/{count}.json"
            shutil.copy(src_bbox, dst_bbox)

            files_3D_bbox = os.listdir(f"{src}/{i}/grabber_{j}/{data_option}/3d_detection/")
            src_3D_bbox = f"{src}/{i}/grabber_{j}/{data_option}/3d_detection/{files_3D_bbox[0]}"
            dst_3D_bbox = f"{data_name}/Bbox_3d/{count}.json"
            shutil.copy(src_3D_bbox, dst_3D_bbox)

            files_bbox_loose = os.listdir(f"{src}/{i}/grabber_{j}/{data_option}/2d_detection_loose/")
            src_bbox_loose = f"{src}/{i}/grabber_{j}/{data_option}/2d_detection_loose/{files_bbox_loose[0]}"
            dst_bbox_loose = f"{data_name}/Bbox_2d_loose/{count}.json"
            shutil.copy(src_bbox_loose, dst_bbox_loose)



