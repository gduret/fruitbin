echo "path_gt: $1"
echo "path_result: $2"

echo "moving features from $1/class/Instance_Mask_resized to $2/id_label/"

#cp -r /gpfsscratch/rech/uli/ubn15wo/GUIMOD_New_low_1/Generated_Worlds_Evaluating/$1/Instance_Mask_resized/* /gpfsscratch/rech/uli/ubn15wo/DenseFusion/datasets/linemod/Linemod_preprocessed/segnet_results/$2_label/

mkdir $2
mkdir $2/01_label_yolo/
mkdir $2/01_label_gt/
cp -r $1/apple2/Instance_Mask_resized/* $2/01_label_gt/
mkdir $2/02_label_yolo/
mkdir $2/02_label_gt/
cp -r $1/apricot/Instance_Mask_resized/* $2/02_label_gt/
mkdir $2/03_label_yolo/
mkdir $2/03_label_gt/
cp -r $1/banana1/Instance_Mask_resized/* $2/03_label_gt/
mkdir $2/04_label_yolo/
mkdir $2/04_label_gt/
cp -r $1/kiwi1/Instance_Mask_resized/* $2/04_label_gt/
mkdir $2/05_label_yolo/
mkdir $2/05_label_gt/
cp -r $1/lemon2/Instance_Mask_resized/* $2/05_label_gt/
mkdir $2/06_label_yolo/
mkdir $2/06_label_gt/
cp -r $1/orange2/Instance_Mask_resized/* $2/06_label_gt/
mkdir $2/07_label_yolo/
mkdir $2/07_label_gt/
cp -r $1/peach1/Instance_Mask_resized/* $2/07_label_gt/
mkdir $2/08_label_yolo/
mkdir $2/08_label_gt/
cp -r $1/pear2/Instance_Mask_resized/* $2/08_label_gt/


echo "moving done"
