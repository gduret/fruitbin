
import numpy as np
import json
from utils import compute_categories_id, compute_id_good_occ
from scipy.spatial.transform import Rotation
from bbox_2d import bbox_2d
import cv2
from instance_mask import instance
from pose import convert2
from matplotlib import image
from fps_alg import process2
import os
from PIL import Image

def process_compute(data_path, full_path, camera, camera_resized, new_size, Nb_camera, World_begin, Nb_world, list_categories, occ_target_min, occ_target_max=1, vis=False):
    transformation = np.matrix([[0.0000000, -1.0000000, 0.0000000],
                                [0.0000000, 0.0000000, -1.0000000],
                                [1.0000000, 0.0000000, 0.0000000]])
    
    scenarios = ["Worlds", "Cameras", "Mix_all", "all"]
    #scenarios = ["all"]

    destination_folders_list = {}
    for scenario_loop in scenarios:
        destination_folders_list[scenario_loop] = [f"Generated_{scenario_loop}_Testing", f"Generated_{scenario_loop}_Evaluating", f"Generated_{scenario_loop}_Training", f"Generated_{scenario_loop}_dont_save" ]
        #destination_folders_list[scenario_loop] = [f"Generated_{scenario_loop}" ]

    list_count_categories = {}
    list_count_categories_filtered = {}
    for scenario_loop in scenarios :
        list_count_categories[scenario_loop] = {}
        list_count_categories_filtered[scenario_loop] = {}
        for destination_folder_loop in destination_folders_list[scenario_loop] : 
            list_count_categories[scenario_loop][destination_folder_loop] = {}
            list_count_categories_filtered[scenario_loop][destination_folder_loop] = {}


    destination_folders = {}
    destination_folders["all"] = f"Generated_all"

    for i in range(World_begin, World_begin + Nb_world): # worlds

        if i > 8000 :
            destination_folders["Worlds"] = f"Generated_Worlds_Testing"
        elif i > 6000 :
            destination_folders["Worlds"] = f"Generated_Worlds_Evaluating"
        else :
            destination_folders["Worlds"] = f"Generated_Worlds_Training"
        
        categories_instance_array_id_to_cat, categories_instance_array_cat_to_id, categories_label_to_id = compute_categories_id(data_path, i)
        
        for j in range(1, Nb_camera+1): # cameras
            p = ((i-1)*Nb_camera) + j

            if j > 12 :
                destination_folders["Cameras"] = f"Generated_Cameras_Testing"
            elif j > 9 :
                destination_folders["Cameras"] = f"Generated_Cameras_Evaluating"
            else :
                destination_folders["Cameras"] = f"Generated_Cameras_Training"

            if i > 8000 and j > 12 :
                destination_folders["Mix_all"] = f"Generated_Mix_all_Testing"
            elif i > 6000 and i <=8000 and j > 9 and j >= 12 :
                destination_folders["Mix_all"] = f"Generated_Mix_all_Evaluating"
            elif i <= 6000 and j <= 9 :
                destination_folders["Mix_all"] = f"Generated_Mix_all_Training"
            else : 
                destination_folders["Mix_all"] = f"Generated_Mix_all_dont_save"

            categories_array_filtered, categories_array_filtered_occ, categories_array_all, categories_array_all_occ = compute_id_good_occ(data_path, p, categories_instance_array_id_to_cat, categories_instance_array_cat_to_id, occ_target_min, occ_target_max)

            ### 3D Poses ###
            with open(f'{data_path}/Pose/{p}.json', 'r') as f:
                data_3D_pose = json.load(f)

            ### 2D BBox ###
            with open(f"{data_path}/Bbox_2d/{p}.json", 'r') as f:
                data_Bbox_2d = json.load(f)

            with open(f"{data_path}/Bbox_3d/{p}.json", 'r') as f:
                data_Bbox_3d = json.load(f)

            if len(data_Bbox_2d) != len(data_3D_pose) :
                raise TypeError("size of datas are differents !!")
                
            for scenario_loop in scenarios:
                for destination_folder_loop in destination_folders_list[scenario_loop] :
                    if os.path.isfile(f'{full_path}/{destination_folder_loop}/Count_{p-1}.json'):
                        with open(f'{full_path}/{destination_folder_loop}/Count_{p-1}.json') as f:
                            list_count_categories[scenario_loop][destination_folder_loop] = json.load(f)
            
            for scenario_loop in scenarios:
                for destination_folder_loop in destination_folders_list[scenario_loop] :
                    if os.path.isfile(f'{full_path}/{destination_folder_loop}/Count_filtered_{p-1}.json'):
                        with open(f'{full_path}/{destination_folder_loop}/Count_filtered_{p-1}.json') as f:
                            list_count_categories_filtered[scenario_loop][destination_folder_loop] = json.load(f)

            #res_all = []
            for categories in list_categories:
                if categories in categories_array_filtered.keys():
                    Nb_instance_all = len(categories_array_all[categories])
                    Nb_instance_occ = len(categories_array_filtered[categories])
                    
                    for scenario_loop in scenarios:

                        meta = {}
                        # 
                        if not destination_folders[scenario_loop] in list_count_categories[scenario_loop].keys():
                            list_count_categories[scenario_loop][destination_folders[scenario_loop]] = {}
                        if not categories in list_count_categories[scenario_loop][destination_folders[scenario_loop]].keys():
                            list_count_categories[scenario_loop][destination_folders[scenario_loop]][categories] = {}
                        if f"{Nb_instance_all}_instances" in list_count_categories[scenario_loop][destination_folders[scenario_loop]][categories].keys() : 
                            list_count_categories[scenario_loop][destination_folders[scenario_loop]][categories][f"{Nb_instance_all}_instances"] += 1
                        else : 
                            list_count_categories[scenario_loop][destination_folders[scenario_loop]][categories][f"{Nb_instance_all}_instances"] = 1
                        if (Nb_instance_occ == 1 and Nb_instance_all == 1): 
                            if "1_instances_filtered" in list_count_categories[scenario_loop][destination_folders[scenario_loop]][categories].keys() : 
                                list_count_categories[scenario_loop][destination_folders[scenario_loop]][categories]["1_instances_filtered"] += 1
                            else : 
                                list_count_categories[scenario_loop][destination_folders[scenario_loop]][categories]["1_instances_filtered"] = 1



                        if not destination_folders[scenario_loop] in list_count_categories_filtered[scenario_loop].keys():
                            list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]] = {}
                        if not categories in list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]].keys():
                            list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]][categories] = {}
                        if f"{Nb_instance_occ}_instances" in list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]][categories].keys() : 
                            list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]][categories][f"{Nb_instance_occ}_instances"] += 1
                        else : 
                            list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]][categories][f"{Nb_instance_occ}_instances"] = 1
                        if (Nb_instance_occ == 1 and Nb_instance_all == 1): 
                            if "1_instances_filtered" in list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]][categories].keys() : 
                                list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]][categories]["1_instances_filtered"] += 1
                            else : 
                                list_count_categories_filtered[scenario_loop][destination_folders[scenario_loop]][categories]["1_instances_filtered"] = 1


                        meta['id_dataset'] = 1
                        meta['id_original'] = p
                        meta['world'] = i
                        meta['camera'] = j
                        meta['id_category'] = categories_label_to_id[categories]
                        meta['id_generated'] = list_count_categories[scenario_loop][destination_folders[scenario_loop]][categories][f"{Nb_instance_all}_instances"]
                        meta['Nb_instance'] = Nb_instance_all
                        meta['all_id_instance'] = categories_array_all[categories] 
                        meta['all_id_instance_occlusion'] = categories_array_all_occ[categories] 
                        meta['target_occlusion_min'] = occ_target_min
                        meta['target_occlusion_max'] = occ_target_max
                        meta['Nb_instance_filtered'] = Nb_instance_occ
                        meta['id_instance_good'] = categories_array_filtered[categories]
                        meta['id_instance_good_occlusion'] = categories_array_filtered_occ[categories]

                        if not os.path.isfile(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/Meta_Gen/{categories}_{p}.json'):
                            with open(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/Meta_Gen/{categories}_{p}.json', mode='w') as f:
                                feeds = {}
                                feeds[meta['id_generated']]=meta
                                f.write(json.dumps(feeds, indent=2))
                        else:
                            with open(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/Meta_Gen/{categories}_{p}.json') as feedsjson:
                                feeds = json.load(feedsjson)
                                feeds[meta['id_generated']]=meta
                            with open(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/Meta_Gen/{categories}_{p}.json', mode='w') as f:
                                f.write(json.dumps(feeds, indent=4))    
                        
                    if (Nb_instance_occ == 1 and Nb_instance_all == 1 ): # condition of only one instance of occ >= 0.5 and no other < 0.05

                        for k in range(len(data_3D_pose)):
                            if data_3D_pose[k]['id'] == categories_array_filtered[categories][0]:
                                rpy = data_3D_pose[k]['pose']['rpy']
                                rot = convert2(rpy)
                                R_exp = transformation @ rot
                                R_exp = np.array(R_exp)

                                xyz = data_3D_pose[k]['pose']['xyz']
                                T_exp = transformation @ xyz
                                T_exp = np.array(T_exp)
                                num_arr = np.c_[R_exp, T_exp[0]]
                                for scenario_loop in scenarios:
                                    if not destination_folders[scenario_loop] == "dont_save" :
                                        np.save(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/Pose_transformed/{p}.npy', num_arr)  # save

                            if data_Bbox_2d[k]['id'] == categories_array_filtered[categories][0]:
                                bbox = bbox_2d(data_Bbox_2d[k])
                                for scenario_loop in scenarios:
                                    if not destination_folders[scenario_loop] == "dont_save" :
                                        np.savetxt(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/Bbox/{p}.txt', np.array(bbox).reshape((1, 4)))  # save

                            if data_Bbox_3d[k]['id'] == categories_array_filtered[categories][0]:
                                bbox3d_size = data_Bbox_3d[k]['bbox']['size']
                                for scenario_loop in scenarios:
                                    if not destination_folders[scenario_loop] == "dont_save" :
                                        np.savetxt(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/Bbox_3d_Gen/{p}.txt', bbox3d_size)  # save

                        id = categories_array_filtered[categories][0]
                        img = cv2.imread(f"{data_path}/Instance_Segmentation/{p}.png", cv2.IMREAD_UNCHANGED) # plt.imread(path)
                        
                        depth = Image.open(f"{data_path}/Depth/{p}.tiff")
                        print(f"{data_path}/Depth/{p}.tiff")                        
                        for scenario_loop in scenarios:
                            if not destination_folders[scenario_loop] == "dont_save" :
                                depth_array = np.asarray(depth.getdata()).reshape(depth.size[1], depth.size[0])
                                depth1 = cv2.resize(np.array(depth), new_size)
                                depth2 = depth1 * 1000
                                depth3 = depth2.astype(np.uint32)
                                resized = Image.fromarray(depth3)
                                depth_high = Image.fromarray(depth_array)  
                                #img2 = np.asarray(res.getdata()).reshape(res.size[1], res.size[0])
                                print(f"{full_path}/{destination_folders[scenario_loop]}/{categories}/Depth_resized/{p}.png")
                                #depth_high.save(f"{data_name}/{destination_folders[scenario_loop]}/{categories}/Depth_Gen/{p}.png")
                                resized.save(f"{full_path}/{destination_folders[scenario_loop]}/{categories}/Depth_resized/{p}.png")

                        instance_img = instance(img, id)
                        for scenario_loop in scenarios:
                            if not destination_folders[scenario_loop] == "dont_save" :
                                cv2.imwrite(f"{full_path}/{destination_folders[scenario_loop]}/{categories}/Instance_Mask/{p}.png", 255*instance_img)

                                id_obj = 0.0
                                res = [id_obj]
                                
                                image = cv2.imread(f"{full_path}/{destination_folders[scenario_loop]}/{categories}/Instance_Mask/{p}.png", 0)
                                image = image/255.0
                                contours, _ = cv2.findContours(image.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
                                if len(contours) > 1 :
                                    continue
                                for l in range(len(contours[0])):
                                    x = contours[0][l][0][0]/640.0
                                    res.append(x)
                                    y = contours[0][l][0][1]/480.0
                                    res.append(y)
                                #id_obj += 1.0
                                #res_all.append(res)

                                a_file = open(f"{full_path}/{destination_folders[scenario_loop]}/{categories}/Labels/{p}.txt", "w")
                                #for row in res_all:
                                np.savetxt(a_file, np.array(res).reshape(1, len(res)))

                                a_file.close()

                        instance_img_resized = cv2.resize(instance_img, new_size)
                        for scenario_loop in scenarios:
                            if not destination_folders[scenario_loop] == "dont_save" :
                                cv2.imwrite(f"{full_path}/{destination_folders[scenario_loop]}/{categories}/Instance_Mask_resized/{p}.png", 255*instance_img_resized)
                        img = cv2.imread(f"{data_path}/RGB/{p}.png")
                        for scenario_loop in scenarios:
                            if not destination_folders[scenario_loop] == "dont_save" :
                                cv2.imwrite(f"{full_path}/{destination_folders[scenario_loop]}/{categories}/RGB_Gen/{p}.png", img)
                        img_resized = cv2.resize(img, new_size)
                        for scenario_loop in scenarios:
                            if not destination_folders[scenario_loop] == "dont_save" :
                                cv2.imwrite(f"{full_path}/{destination_folders[scenario_loop]}/{categories}/RGB_resized/{p}.png", img_resized)

                        np.set_printoptions(precision=15)
                        #for scenario_loop in scenarios:
                        for scenario_loop in scenarios:
                            if not destination_folders[scenario_loop] == "dont_save" :
                                pose = np.load(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/Pose_transformed/{p}.npy')
                                R_exp = pose[0:3, 0:3]
                                tVec = pose[0:3, 3]

                                fps_points = np.loadtxt(f'{full_path}/Generated/{categories}/{categories}_fps_3d.txt')
                                center = fps_points.mean(0)
                                fps_points = np.append(fps_points, [center], axis=0)
                                points = process2(fps_points, R_exp, tVec, camera, img, vis)
                                out = [int(categories_array_filtered[categories][0])] #len have to be 1
                                ind = 1
                                for point in points:
                                    x = point[0][0] / img.shape[1]
                                    y = point[0][1] / img.shape[0]
                                    out.append(x)
                                    out.append(y)
                                    ind += 2
                                np.savetxt(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/FPS/{p}.txt',  np.array(out).reshape(1, len(out)))

                        points_resized = process2(fps_points, R_exp, tVec, camera_resized, img_resized, vis)
                        out_resized = [int(categories_array_filtered[categories][0])] #len have to be 1 !
                        ind_resized = 1
                        for point_resized in points_resized:
                            x_resized = point_resized[0][0] / img_resized.shape[1]
                            y_resized = point_resized[0][1] / img_resized.shape[0]
                            out_resized.append(x_resized)
                            out_resized.append(y_resized)
                            ind_resized += 2
                        for scenario_loop in scenarios:
                            if not destination_folders[scenario_loop] == "dont_save" :
                                np.savetxt(f'{full_path}/{destination_folders[scenario_loop]}/{categories}/FPS_resized/{p}.txt',  np.array(out_resized).reshape(1, len(out_resized)))
    

    for scenario_loop in scenarios:
        for destination_folder_loop in destination_folders_list[scenario_loop] : # [f"Generated_{scenario}_Testing", f"Generated_{scenario}_Evaluating", f"Generated_{scenario}_Training"] :
            with open(f'{full_path}/{destination_folder_loop}/Count_{p}.json', mode='w') as f:
                f.write(json.dumps(list_count_categories[scenario_loop][destination_folder_loop], indent=4))
    with open(f'{full_path}/Count_{p}.json', mode='w') as f:
                f.write(json.dumps(list_count_categories, indent=4))
    print(list_count_categories)
    print(f'{full_path}/{destination_folder_loop}/Count_{p}.json')

