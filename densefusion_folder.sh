echo "Class: $1";
echo "id:: $2";
echo "moving features from /gpfsscratch/rech/uli/ubn15wo/GUIMOD_New_low_1/Generated_Worlds_Training/$1 to /gpfsscratch/rech/uli/ubn15wo/datasets/linemod/Linemod_preprocessed/data/$2"

cp -r /gpfsscratch/rech/uli/ubn15wo/GUIMOD_New_low_1/Generated_Worlds_Training/$1/Depth_resized/* /gpfsscratch/rech/uli/ubn15wo/DenseFusion/datasets/linemod/Linemod_preprocessed/data/$2/depth/
cp -r /gpfsscratch/rech/uli/ubn15wo/GUIMOD_New_low_1/Generated_Worlds_Evaluating/$1/Depth_resized/* /gpfsscratch/rech/uli/ubn15wo/DenseFusion/datasets/linemod/Linemod_preprocessed/data/$2/depth/
cp -r /gpfsscratch/rech/uli/ubn15wo/GUIMOD_New_low_1/Generated_Worlds_Evaluating/$1/RGB_resized/* /gpfsscratch/rech/uli/ubn15wo/DenseFusion/datasets/linemod/Linemod_preprocessed/data/$2/rgb/
cp -r /gpfsscratch/rech/uli/ubn15wo/GUIMOD_New_low_1/Generated_Worlds_Training/$1/RGB_resized/* /gpfsscratch/rech/uli/ubn15wo/DenseFusion/datasets/linemod/Linemod_preprocessed/data/$2/rgb/
cp -r /gpfsscratch/rech/uli/ubn15wo/GUIMOD_New_low_1/Generated_Worlds_Training/$1/Instance_Mask_resized/* /gpfsscratch/rech/uli/ubn15wo/DenseFusion/datasets/linemod/Linemod_preprocessed/data/$2/mask/
cp -r /gpfsscratch/rech/uli/ubn15wo/GUIMOD_New_low_1/Generated_Worlds_Evaluating/$1/Instance_Mask_resized/* /gpfsscratch/rech/uli/ubn15wo/DenseFusion/datasets/linemod/Linemod_preprocessed/data/$2/mask/

echo "moving done"

