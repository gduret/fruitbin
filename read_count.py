import os
import numpy as np
from prepare_data import reform_data
from fps_alg import apply_fps
from bbox_3d import get_3D_bbox
from compute_features import process_compute
import open3d as o3d
from scipy.spatial import distance
import argparse
import json
import pandas as pd
import altair as alt


def generate_folders(name, list_categories, scenario):
    is_exist = os.path.exists(name)
    if not is_exist:
        os.mkdir(name)
    folders = ["RGB", "RGB_Gen", "RGB_resized", "Meta_Gen", "Depth", "Mask", "Meta", "Pose", "Bbox_2d", "Bbox_2d_loose", "Bbox_3d", "Bbox_3d_Gen",  "Instance_Segmentation", "Semantic_Segmentation", "Instance_Mask", "Instance_Mask_resized", "Occlusion", "Models", "Pose_transformed", "Bbox", "FPS", "FPS_resized"]
    for f in folders:
        is_exist = os.path.exists(f"{name}/{f}")
        if not is_exist:
            if f not in ["RGB_Gen", "RGB_resized",  "Instance_Mask", "Instance_Mask_resized", "Meta_Gen", "Models", "Pose_transformed", "Bbox", "Bbox_3d_Gen", "FPS" , "FPS_resized"]:
                os.mkdir(f"{name}/{f}")
            else:
                for cat in list_categories:
                    is_exist2 = os.path.exists(f"{name}/Generated/{cat}")
                    if not is_exist2:
                        os.makedirs(f"{name}/Generated/{cat}")
                    is_exist2 = os.path.exists(f"{name}/Generated/{cat}/Pose_transformed")
                    if not is_exist2:
                        os.makedirs(f"{name}/Generated/{cat}/Pose_transformed")
                    for scenario in ["Worlds", "Cameras", "Mix_all"] :
                        is_exist2 = os.path.exists(f"{name}/Generated_{scenario}_Training/{cat}/{f}")
                        if not is_exist2:
                            os.makedirs(f"{name}/Generated_{scenario}_Training/{cat}/{f}")
                        is_exist2 = os.path.exists(f"{name}/Generated_{scenario}_Evaluating/{cat}/{f}")
                        if not is_exist2:
                            os.makedirs(f"{name}/Generated_{scenario}_Evaluating/{cat}/{f}")
                        is_exist2 = os.path.exists(f"{name}/Generated_{scenario}_Testing/{cat}/{f}")
                        if not is_exist2:
                            os.makedirs(f"{name}/Generated_{scenario}_Testing/{cat}/{f}")
                        is_exist2 = os.path.exists(f"{name}/Generated_{scenario}_dont_save/{cat}/{f}")
                        if not is_exist2:
                            os.makedirs(f"{name}/Generated_{scenario}_dont_save/{cat}/{f}")



def calc_pts_diameter2(pts):
    """Calculates the diameter of a set of 3D points (i.e. the maximum distance
  between any two points in the set). Faster but requires more memory than
  calc_pts_diameter.
  :param pts: nx3 ndarray with 3D points.
  :return: The calculated diameter.
  """
    dists = distance.cdist(pts, pts, 'euclidean')
    diameter = np.max(dists)
    return diameter


def prep_df(df, name):
    df = df.stack().reset_index()
    df.columns = ['c1', 'c2', 'values']
    df['DF'] = name
    return df



if __name__ == '__main__':    
    # Create the parser
    parser = argparse.ArgumentParser()
    # Parse the argument
    args = parser.parse_args()

    scenario = "Worlds"

    ### parameters ###
    Categories = [] # to read
    Nb_instance = 1
    occ_target = 0.5

    dataset_src = "/gpfsscratch/rech/uli/ubn15wo/dataset/s2rg/Fruits_all_medium/data/"
    #dataset_src = "/media/mahmoud/E/Fruits_easy/data"
    #dataset_src = "/media/gduret/DATA/dataset/s2rg/Fruits_all_medium/data"

    choice = "low" # depth of rgb resolution datas
    data_options = {"high": "ground_truth_rgb",
                    "low": "ground_truth_depth"}
    dataset_type = data_options[choice]
    dataset_name = f"/gpfsscratch/rech/uli/ubn15wo/dataset/s2rg/Fruits_all_medium/GUIMOD_{choice}"
    list_categories = [ "apple2" , "apricot", "banana1", "kiwi1", "lemon2", "orange2", "peach1", "pear2"]


    path_json = "Count_150000.json"

    if os.path.isfile(path_json):
        with open(path_json) as f:
            list_count_categories = json.load(f)

    scenarios = ["Worlds", "Cameras", "Mix_all"]

    destination_folders_list = {}
    for scenario_loop in scenarios:
        destination_folders_list[scenario_loop] = [f"Generated_{scenario_loop}_Testing", f"Generated_{scenario_loop}_Evaluating", f"Generated_{scenario_loop}_Training", f"Generated_{scenario_loop}_dont_save" ]


    print(list_count_categories)

    stat_cat = {}

    worlds_train=[]
    worlds_eval=[]
    worlds_test=[]
    cameras_train=[]
    cameras_eval=[]
    cameras_test=[]
    mix_train=[]
    mix_eval=[]
    mix_test=[]

    stat_cat_inst = {}

    for scenario_loop in scenarios :
        stat_cat_inst[scenario_loop] = {}
        for cat in list_categories :
            stat_cat_inst[scenario_loop][cat] = {}

    for cat in list_categories : 
        stat_cat[cat] = {}
        for scenario_loop in scenarios :
            stat_cat[cat][scenario_loop] = {}
            for destination_folder_loop in destination_folders_list[scenario_loop] : # [f"Generated_{scenario}_Testing", f"Generated_{scenario}_Evaluating", f"Generated_{scenario}_Training"] :
                #print("scenario_loop : " , scenario_loop)
                #print("destination_folder_loop : " , destination_folder_loop)
                #print("cat : " , cat)
                #print("list_count_categories[scenario_loop][destination_folder_loop][cat][1_instances] : " , list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                #print(list_count_categories[scenario_loop][destination_folder_loop])
                
                if destination_folder_loop == "Generated_Worlds_Testing" : 
                    print("\nGenerated_Wolrds_Testing1")
                    worlds_test.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                if destination_folder_loop == "Generated_Cameras_Testing" :
                    print("\nGenerated_Wolrds_Testing2")
                    cameras_test.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                if destination_folder_loop == "Generated_Mix_all_Testing" :
                    print("\nGenerated_Wolrds_Testing3")
                    mix_test.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                if destination_folder_loop == "Generated_Worlds_Evaluating" :
                    print("\nGenerated_Wolrds_Testing4")
                    worlds_eval.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                if destination_folder_loop == "Generated_Cameras_Evaluating" :
                    print("\nGenerated_Wolrds_Testing5")
                    cameras_eval.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                if destination_folder_loop == "Generated_Mix_all_Evaluating" :
                    print("\nGenerated_Wolrds_Testing6")
                    mix_eval.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                if destination_folder_loop == "Generated_Worlds_Training" :
                    print("\nGenerated_Wolrds_Testing7")
                    worlds_train.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                if destination_folder_loop == "Generated_Cameras_Training" :
                    print("\nGenerated_Wolrds_Testing8")
                    cameras_train.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])
                if destination_folder_loop == "Generated_Mix_all_Training" :
                    print("\nGenerated_Wolrds_Testing9")
                    mix_train.append(list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"])


                if cat in list_count_categories[scenario_loop][destination_folder_loop].keys() :
                    stat_cat[cat][scenario_loop][destination_folder_loop] = list_count_categories[scenario_loop][destination_folder_loop][cat]["1_instances"]
                #print(list_count_categories[scenario_loop][destination_folder_loop].keys())
                if cat in list_count_categories[scenario_loop][destination_folder_loop].keys() : # because dont save empty for world and camera scenario
                    for nb_inst in list_count_categories[scenario_loop][destination_folder_loop][cat].keys() : 
                        if nb_inst in stat_cat_inst[scenario_loop][cat].keys() :
                            stat_cat_inst[scenario_loop][cat][nb_inst] = stat_cat_inst[scenario_loop][cat][nb_inst] + list_count_categories[scenario_loop][destination_folder_loop][cat][nb_inst]
                        else: 
                            stat_cat_inst[scenario_loop][cat][nb_inst] = list_count_categories[scenario_loop][destination_folder_loop][cat][nb_inst]
    #print(stat_cat)

    print(stat_cat_inst)

    print(worlds_train)
    print(worlds_eval)
    print(worlds_test)
    print(cameras_train)
    print(cameras_eval)
    print(cameras_test)
    print(mix_train)
    print(mix_eval)
    print(mix_test)

    list_Nb_instance = [ "0_instances" , "1_instances", "2_instances", "3_instances", "4_instances", "5_instances" , 
                        "6_instances", "7_instances" , "8_instances", "9_instances","10_instances","11_instances",
                        "12_instances", "13_instances" , "14_instances", "15_instances","16_instances","17_instances", "18_instances"]
    
    array_apple=[]
    array_apricot=[]
    array_banana=[]
    array_kiwi=[]
    array_lemon=[]
    array_orange=[]
    array_peach=[]
    array_pear=[]

    list_categories = [ "apple2" , "apricot", "banana1", "kiwi1", "lemon2", "orange2", "peach1", "pear2"]


    for cat in list_categories :
        for nb_inst in list_Nb_instance :
            print(stat_cat_inst[scenario_loop][cat].keys())
            
            if nb_inst in stat_cat_inst[scenario_loop][cat].keys() : #because some rare occurance of large instance number
                if cat == "apple2" :
                    array_apple.append(stat_cat_inst[scenario_loop][cat][nb_inst])
                if cat == "apricot" :
                    array_apricot.append(stat_cat_inst[scenario_loop][cat][nb_inst])
                if cat == "banana1" :
                    array_banana.append(stat_cat_inst[scenario_loop][cat][nb_inst])
                if cat == "kiwi1" :
                    array_kiwi.append(stat_cat_inst[scenario_loop][cat][nb_inst])
                if cat == "lemon2" :
                    array_lemon.append(stat_cat_inst[scenario_loop][cat][nb_inst])
                if cat == "orange2" :
                    array_orange.append(stat_cat_inst[scenario_loop][cat][nb_inst])
                if cat == "peach1" :
                    array_peach.append(stat_cat_inst[scenario_loop][cat][nb_inst])
                if cat == "pear2" :
                    array_pear.append(stat_cat_inst[scenario_loop][cat][nb_inst])
            else : #because some rare occurance of large instance number
                print(nb_inst)
                if cat == "apple2" :
                    array_apple.append(0)
                if cat == "apricot" :
                    array_apricot.append(0)
                if cat == "banana1" :
                    array_banana.append(0)
                if cat == "kiwi1" :
                    array_kiwi.append(0)
                if cat == "lemon2" :
                    array_lemon.append(0)
                if cat == "orange2" :
                    array_orange.append(0)
                if cat == "peach1" :
                    array_peach.append(0)
                if cat == "pear2" :
                    array_pear.append(0)
                    
    print()

    print(array_apple)
    print(array_apricot)
    print(array_banana)
    print(array_kiwi)
    print(array_lemon)
    print(array_orange)
    print(array_peach)
    print(array_pear)
    print(len(array_pear))
    print(np.resize( array_apple , (3, 19)))

    df_apple=pd.DataFrame(np.resize( array_apple , (1, 19)),index=["Dataset"],columns=list_Nb_instance)
    df_apricot=pd.DataFrame(np.resize( array_apricot , (1, 19)),index=["Dataset"],columns=list_Nb_instance)
    df_banana=pd.DataFrame(np.resize( array_banana , (1, 19)),index=["Dataset"],columns=list_Nb_instance)
    df_kiwi=pd.DataFrame(np.resize( array_kiwi , (1, 19)),index=["Dataset"],columns=list_Nb_instance)
    df_lemon=pd.DataFrame(np.resize( array_lemon , (1, 19)),index=["Dataset"],columns=list_Nb_instance)
    df_orange=pd.DataFrame(np.resize( array_orange , (1, 19)),index=["Dataset"],columns=list_Nb_instance)
    df_peach=pd.DataFrame(np.resize( array_peach , (1, 19)),index=["Dataset"],columns=list_Nb_instance)
    df_pear=pd.DataFrame(np.resize( array_pear , (1, 19)),index=["Dataset"],columns=list_Nb_instance)

    df_apple1 = prep_df(df_apple, 'Apple')
    df_apricot1 = prep_df(df_apricot, 'Apricot')
    df_banana1 = prep_df(df_banana, 'Banana')
    df_kiwi1 = prep_df(df_kiwi, 'Kiwi')
    df_lemon1 = prep_df(df_lemon, 'Lemon')
    df_orange1 = prep_df(df_orange, 'Orange')
    df_peach1 = prep_df(df_peach, 'Peach')
    df_pear1 = prep_df(df_pear, 'Pear')

    df = pd.concat([df_apple1, df_apricot1, df_banana1, df_kiwi1, df_lemon1, df_orange1, df_peach1, df_pear1])

    # qxqxs

    # df1_train=pd.DataFrame(np.resize(np.concatenate((np.array(worlds_train), np.array(cameras_train), np.array(mix_train)) ), (3,8)),index=["World", "Cameras", "All"],columns=[ "apple" , "apricot", "banana", "kiwi", "lemon", "orange", "peach", "pear"])
    # df2_eval=pd.DataFrame(np.resize(np.concatenate((np.array(worlds_eval), np.array(cameras_eval), np.array(mix_eval)) ), (3,8)),index=["World", "Cameras", "All"],columns=[ "apple" , "apricot", "banana", "kiwi", "lemon", "orange", "peach", "pear"])
    # df3_test=pd.DataFrame(np.resize(np.concatenate((np.array(worlds_test), np.array(cameras_test), np.array(mix_test)) ), (3,8)),index=["World", "Cameras", "All"],columns=[ "apple" , "apricot", "banana", "kiwi", "lemon", "orange", "peach", "pear"])


    # df1 = prep_df(df1_train, 'Train')
    # df2 = prep_df(df2_eval, 'Eval')
    # df3 = prep_df(df3_test, 'Test')

    # df = pd.concat([df1, df2, df3])


    # print(np.resize(np.concatenate((np.array(worlds_train), np.array(cameras_train), np.array(mix_train)) ), (3,8)))
    # print(np.random.rand(4,3))

    # df1=pd.DataFrame(10*np.random.rand(4,3),index=["A","B","C","D"],columns=["I","J","K"])
    # df2=pd.DataFrame(10*np.random.rand(4,3),index=["A","B","C","D"],columns=["I","J","K"])
    # df3=pd.DataFrame(10*np.random.rand(4,3),index=["A","B","C","D"],columns=["I","J","K"])

    # def prep_df(df, name):
    #     df = df.stack().reset_index()
    #     df.columns = ['c1', 'c2', 'values']
    #     df['DF'] = name
    #     return df

    # df1 = prep_df(df1, 'DF1')
    # df2 = prep_df(df2, 'DF2')
    # df3 = prep_df(df3, 'DF3')

    # df = pd.concat([df1, df2, df3])

    alt.renderers.enable('altair_viewer')

    chart = alt.Chart(df).mark_bar().encode(
    
    
    # tell Altair which field to group columns on
    x=alt.X('c2:N', title=None, sort=None),

    # tell Altair which field to use as Y values and how to calculate
    y=alt.Y('sum(values):Q',
        axis=alt.Axis(
            grid=False,
            title=None)),

    # tell Altair which field to use to use as the set of columns to be  represented in each group
    column=alt.Column('c1:N', title=None),

    # tell Altair which field to use for color segmentation 
    color=alt.Color('DF:N',
            scale=alt.Scale(
                # make it look pretty with an enjoyable color pallet
                range=['#96ceb4', '#ffcc5c','#ff6f69'],
            ),
        ))\
    .configure_view(
        # remove grid lines around column clusters
        strokeOpacity=0    
    )

    chart.show()


    with open(f'Count_Stat.json', mode='w') as f:
        f.write(json.dumps(stat_cat, indent=4))
    with open(f'Count_Stat_instance.json', mode='w') as f:
        f.write(json.dumps(stat_cat_inst, indent=4))


































