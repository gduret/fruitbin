import math
import numpy as np
import json
from scipy.spatial.transform import Rotation


def compute_categories_id(data_name, world):
    #Category = 'banana1'
    #Category = 'pear2'
    #Category = "orange2"
    # Opening JSON file
    f = open(f'{data_name}/Meta/{world}.json')
    
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
    
    # Iterating through the json
    # list

    catergories_label_to_id={}
    catergories_id_to_label={}
    catergories_instance_array_cat_to_id={}
    catergories_instance_array_id_to_cat={}

    for k in data['categories']:
        catergories_label_to_id[k['label']]=k['id']
        catergories_id_to_label[k['id']]=k['label']
        catergories_instance_array_cat_to_id[k['label']]=[]

    for k in data['objects']:
        #print(k)
        #catergories_instance_array[catergories_id_to_label[i['category_id']]]
        catergories_instance_array_id_to_cat[k['id']] = catergories_id_to_label[k['category_id']]
        catergories_instance_array_cat_to_id[catergories_id_to_label[k['category_id']]].append(k['id'])
        # if i['category_id'] == id_category :
        #     print("Hello fruits instance")
        #     id_instances.append(i['id'])
        #     print(i['id']) 

    # Closing file
    f.close()

    return catergories_instance_array_id_to_cat, catergories_instance_array_cat_to_id



def compute_id_good_occ(data_name, count, catergories_instance_array_id_to_cat, catergories_instance_array_cat_to_id, Occ_wanted):

    f2 = open(f'{data_name}/Occlusion/{count}.json')

    data2 = json.load(f2)
    catergories_occ_array = {}

    for cat in catergories_instance_array_cat_to_id :
        #print(cat)
        catergories_occ_array[cat] = []

    for i in data2:
        if i['occlusion_value'] > 0.5 :
            catergories_occ_array[catergories_instance_array_id_to_cat[i['id']]].append(i['id'])

    # Closing file
    f2.close()

    return catergories_occ_array


def convert2(xyz):
    (R, P, Y) = (xyz[0], xyz[1], xyz[2])
    Q = Rotation.from_euler(seq='xyz', angles=[R, P, Y], degrees=False).as_quat()
    r = Rotation.from_quat(Q)
    rotation = r.as_matrix()

    return rotation


def transform_pose(data_name, Nb_camera, Nb_world, list_categories, occ_target):
    transformation = np.matrix([[0.0000000, -1.0000000, 0.0000000],
                                [0.0000000, 0.0000000, -1.0000000],
                                [1.0000000, 0.0000000, 0.0000000]])
    cont1, cont2, cont3 = 0, 0, 0
    for i in range(1, Nb_world + 1): # worlds
        
        catergories_instance_array_id_to_cat, catergories_instance_array_cat_to_id = compute_categories_id(data_name, i)
        
        for j in range(1, Nb_camera+1): # cameras
            p = ((i-1)*Nb_camera) + j

            catergories_occ_array = compute_id_good_occ(data_name, p, catergories_instance_array_id_to_cat, catergories_instance_array_cat_to_id, occ_target)


            ### Generate Poses ###

            with open(f'{data_name}/Pose/{p}.json', 'r') as f:
                data = json.load(f)
            for k in range(len(data)):

                for categories in list_categories:

                    if len(catergories_occ_array[categories]) == 1 and data[k]['id'] == catergories_occ_array[categories][0]:
                        cont1 += 1
                        rpy = data[k]['pose']['rpy']
                        rot = convert2(rpy)
                        R_exp = transformation @ rot
                        R_exp = np.array(R_exp)

                        xyz = data[k]['pose']['xyz']
                        T_exp = transformation @ xyz
                        T_exp = np.array(T_exp)
                        num_arr = np.c_[R_exp, T_exp[0]]
                        np.save(f'{data_name}/Generated/Pose_transformed/{categories}/{p}.npy', num_arr)  # save
                    else:
                        continue

    print(cont1, cont2, cont3)

