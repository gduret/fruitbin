
import json


def compute_categories_id(data_name, world):

    # Opening JSON file
    f = open(f'{data_name}/Meta/{world}.json')
    
    # returns JSON object as 
    # a dictionary
    print(f'{data_name}/Meta/{world}.json')
    data = json.load(f)
    
    # Iterating through the json
    # list

    categories_label_to_id={}
    categories_id_to_label={}
    categories_instance_array_cat_to_id={}
    categories_instance_array_id_to_cat={}

    for k in data['categories']:
        categories_label_to_id[k['label']]=k['id']
        categories_id_to_label[k['id']]=k['label']
        categories_instance_array_cat_to_id[k['label']]=[]

    for k in data['objects']:
        categories_instance_array_id_to_cat[k['id']] = categories_id_to_label[k['category_id']]
        categories_instance_array_cat_to_id[categories_id_to_label[k['category_id']]].append(k['id'])

    # Closing file
    f.close()

    return categories_instance_array_id_to_cat, categories_instance_array_cat_to_id, categories_label_to_id



def compute_id_good_occ(data_name, count, categories_instance_array_id_to_cat, categories_instance_array_cat_to_id, occ_target_min, occ_target_max):

    f2 = open(f'{data_name}/Occlusion/{count}.json')

    data2 = json.load(f2)
    categories_array_filtered = {}
    categories_array_filtered_occ = {}

    categories_array_all = {}
    categories_array_all_occ = {}

    for cat in categories_instance_array_cat_to_id :
        categories_array_filtered[cat] = []
        categories_array_filtered_occ[cat] = []
        categories_array_all[cat] = []
        categories_array_all_occ[cat] = []

    for i in data2:
        if i['occlusion_value'] > occ_target_min and i['occlusion_value'] <= occ_target_max :
            categories_array_filtered[categories_instance_array_id_to_cat[i['id']]].append(i['id'])
            categories_array_filtered_occ[categories_instance_array_id_to_cat[i['id']]].append(i['occlusion_value'])
        if  i['occlusion_value'] >= 0.05 :
            categories_array_all[categories_instance_array_id_to_cat[i['id']]].append(i['id'])
            categories_array_all_occ[categories_instance_array_id_to_cat[i['id']]].append(i['occlusion_value'])

    # Closing file
    f2.close()

    #print(categories_array_filtered)
    #print(categories_array_filtered_occ)
    #print(categories_array_all)
    #print(categories_array_all_occ)


    return categories_array_filtered, categories_array_filtered_occ, categories_array_all, categories_array_all_occ
