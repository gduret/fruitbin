import math
import numpy as np
import json

def bbox_2d(data_sample):

    center = data_sample['bbox']['center']
    size = data_sample['bbox']['size']
    bbox_res = []
    top_left_x, top_left_y = center[0] - (size[0] / 2), center[1] - (size[1] / 2)
    bbox_res.append(top_left_x)
    bbox_res.append(top_left_y)

    bottom_right_x, bottom_right_y = center[0] + (size[0] / 2), center[1] + (size[1] / 2)
    bbox_res.append(bottom_right_x)
    bbox_res.append(bottom_right_y)

    return bbox_res


# def generate_2d_bbox(data_name, Nb_camera, Nb_world, list_categories, occ_target):

#     cont1, cont2, cont3 = 0, 0, 0
#     num_arr = 0


#     for i in range(1, Nb_world + 1): # worlds

#         catergories_instance_array_id_to_cat, catergories_instance_array_cat_to_id = compute_categories_id(data_name, i)
        
#         for j in range(1, Nb_camera+1): # cameras
#             p = ((i-1)*Nb_camera) + j

#             catergories_occ_array = compute_id_good_occ(data_name, p, catergories_instance_array_id_to_cat, catergories_instance_array_cat_to_id, occ_target)
            
#             with open(f"{data_name}/Bbox_2d/{p}.json", 'r') as f:
#                 data_Bbox_2d = json.load(f)

#             for k in range(len(data_Bbox_2d)):

#                 for categories in list_categories:
                    
#                     if len(catergories_occ_array[categories]) == 1 and data_Bbox_2d[k]['id'] == catergories_occ_array[categories][0]:

#                         cont1 += 1
#                         bbox = bbox_2d(data_Bbox_2d[k])
#                         np.savetxt(f'{data_name}/Generated/Bbox/{categories}/{p}.txt', np.array(bbox).reshape((1, 4)))  # save
#                     else:
#                         continue
#     print(cont1, cont2, cont3)

