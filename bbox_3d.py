import numpy as np

def get_3D_bbox(ext):
    pcd_bbox = np.array([
        [ext[0], ext[1], ext[2]],
        [- ext[0], ext[1], ext[2]],
        [ext[0], - ext[1], ext[2]],
        [- ext[0], - ext[1], ext[2]],
        [ext[0], ext[1], - ext[2]],
        [- ext[0], ext[1], - ext[2]],
        [ext[0], - ext[1], - ext[2]],
        [- ext[0], - ext[1], - ext[2]]
    ])
    return pcd_bbox

def generate_3d_bbox(data_name):
    objs = {"Banana": [0.029497003182768822, 0.15110498666763306, 0.060593008995056152],
            "Orange": [0.073495000600814819, 0.075856998562812805, 0.074581995606422424],
            "Pear": [0.066010989248752594, 0.12873399630188942, 0.06739199161529541]}
    for key, val in objs.items():
        ext = [x / 2 for x in val]
        bbox = get_3D_bbox(ext)
        np.savetxt(f'{data_name}/Generated/Bbox/{key}_bbox_3d.txt', bbox)
        # print(ext)

